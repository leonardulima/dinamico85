<!-- Usuario efetua o login -->

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Site Dinâmico - Login</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div  id="box-login">
        <div id="formulario-login">
            <form id="frmlogin" name="frmlogin" action="admin/op_usuario.php" method="POST">
                <fieldset>
                    <legend>Faça Login</legend>
                    <br>
                    <label for=""><span>Email</span></label>
                    <input type="text" name="email_usuario" id="email_usuario">
                    <br>
                    <br>
                    <label for=""><span>Senha</span></label>
                    <input type="password" name="senha_usuario" id="senha_usuario">
                    <br>
                    <br>
                    <input type="submit" name="btn_logar_usuario" id="logar" value="logar" class="botao">
                    <br>
                    <br>
                    <span>
                    <?php
                             if(isset($_GET['msg']))
                            {
                                if($_GET['msg']=='ok')
                                 {
                                    echo 'Sucesso';
                                 }
                                 else if ($_GET['msg']=='errokey')
                                {
                                    echo 'As senhas devem ser iguais';
                                }
                                else
                                {
                                    echo 'Erro';
                                }
                            }
                        ?>
                    </span>        
                </fieldset>
            </form>
        </div>
    </div>
    
</body>
</html>
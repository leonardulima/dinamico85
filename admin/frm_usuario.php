<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">    
    <title>Cadastro de Usuario</title>
</head>
<body>        
    <div id="box-cadastro">
        <div id="formulario-menor">
            <form action="op_usuario.php" method="post" enctype="multipart/form-data" name="cadastro_form">
                <legend>Novo Usuario</legend>
                <fieldset>
                    Nome: <br>
                    <input type="text" name="nome" class="form-control"><br><br>
                    Email: <br>
                    <input type="email" name="email" class="form-control"><br><br>
                    Senha: <br>
                    <input type="password" name="senha" class="form-control"><br><br>
                    Confirmar senha<br>
                    <input type="password" id="txt_senha_confirmar" name="txt_senha_confirmar" required>
                    </label>
                    <br>
                    <br>
                    <input type="submit" name="cadastrar_usuario" value="Cadastrar" >

                </fieldset>
            </form> 
        </div>
    </div>
</body>
</html>
<?php
    require_once('../config.php');            
    $users = Usuario::ListarUsers();
    if(count($users) > 0)
    {
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Lista Usuario</title>
</head>
<body>
    <table width='100%' border="" cellpadding="0" cellspacing="1" bgcolor="">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2" align="rigth"><font size="2" color="#fff">ID</font></th>
            <th width="25%" height="2" align="rigth"><font size="2" color="#fff">Nome</font></th>
            <th width="25%" height="2" align="rigth"><font size="2" color="#fff">Email</font></th>
            <th colspan="2" align="center"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
            foreach($users as $user)
            {
        ?>
        <tr>
            <td><?php echo $user['id']?></td>
            <td><?php echo $user['nome']?></td>
            <td><?php echo $user['email']?></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000">
                <a href="<?php echo 'alterar_usuario.php?id='.$user['id'].'&nome='.$user['nome'].'&email='.$user['email']?>">Alterar</a>
            </font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#000">
                <a href="<?php echo "op_usuario.php?excluir=1&id=".$user['id']?>">Excluir</a>
            </font></td>
        </tr>
        <?php
            }
        }
        ?>
    </table>
</body>
</html>
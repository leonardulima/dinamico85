<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alterar Banner</title>
</head>
<body>
    <form action="op_banner.php?update=1" method="post">    
        <fieldset>
            <legend>Alterando Banner</legend>
                <div>
                    <input type="hidden" name="id" value="<?php echo filter_input(INPUT_GET,'id_banner');?>">
                </div>
                <div>
                    <label for="">Titulo</label>
                    <input type="text" name="titulo" value="<?php echo filter_input(INPUT_GET,'titulo');?>">
                </div>
                <br>
                <div>
                    <label for="">Link</label>
                    <input type="text" name="link" value="<?php echo filter_input(INPUT_GET,'link');?>">
                </div>
                <br>
                <div>
                    <label for="">Img</label><br>
                    <input type="file" name="img_banner" value="">
                    <img src="foto/<?php echo $_GET['img'];?>" alt="" width="100" height="100">
                    <input type="hidden" id="img_atual" name="img_atual" values="<?php echo $_GET['img'];?>">
                </div>
                <br>
                <div>
                    <label for="">alt</label>
                    <input type="text" name="alt" value="<?php echo filter_input(INPUT_GET,'alt');?>">
                </div>
                <br>            
                <div>
                    <label for="">Ativo</label>
                    <input type="checkbox" name="ativo" <?php echo filter_input(INPUT_GET,'ativo')=='s'?'checked':'';?>>
                </div>
                <br>
                <br>
                <div>
                    <input type="submit" name="btn_alterar_banner" value="Alterar Banner">
                </div>
        </fieldset>
    </form>
</body>
</html>
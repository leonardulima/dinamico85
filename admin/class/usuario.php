<?php
    class Usuario
    {
        private $id;
        private $nome;
        private $email;
        private $senha;
        
        public function setId($value)
        {
            $this->id = $value;
        }
        public function getId()
        {
            return $this->id;
        }
        public function setNome($value)
        {
            $this->nome = $value;
        }
        public function getNome()
        {
            return $this->nome;
        }
        public function setEmail($value)
        {
            $this->email = $value;
        }
        public function getEmail()
        {
            return $this->email;
        }       
        public function setSenha($value)
        {
            $this->senha = $value;
        }
        public function getSenha()
        {
            return $this->senha;
        }

        public function insertUser()
        {
            $sql = new Sql();
            $result = $sql->select('call sp_usuario_insert (:nome,:email,:senha)',
            array(
            ':nome'=>$this->getNome(),
            ':email'=>$this->getEmail(),
            ':senha'=>md5($this->getSenha())
            ));

            if(count($result)>0)
            {
                $this->setData($result[0]);
            }
        }

        public function updateUser($_id,$_nome,$_email)
        {
            $sql = new Sql();
            $sql->query('UPDATE usuario SET nome = :nome, email = :email, senha = :senha WHERE id = :id',
            array
            (  
                ':nome'=>$_nome,
                ':email'=>$_email,
                ':senha'=>$_senha,
                ':id'=>$_id)
            );
        }
        public function deleteUser($_id)
        {
            $sql = new Sql();
            $sql->query('delete from usuario where id = :id',array(':id'=>$_id));
        }
        public function listarUsers()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM usuario ORDER BY nome');            
        }
        public function consultarUserId($_id)
        {
            $sql = new Sql();
            return $sql->select('select * from usuario where id = :id',array(':id'=>$_id));   
        }
        public function logarUser($_email,$_senha)
        {
            $sql = new Sql();
            $senhaCript = md5($_senha);
            $results = $sql->select('select * from usuario where email = :email AND senha = :senha',array(':email' => $_email,':senha' => $senhaCript));
            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }
        public function setData($dados)
        {
            $this->id = $dados['id'];
            $this->nome = $dados['nome'];
            $this->email = $dados['email'];
            $this->senha = $dados['senha'];
        }
        public function __construct($_id='',$_nome='',$_email='',$_senha='')
        {
            $this->id = $_id;
            $this->nome = $_nome;
            $this->email = $_email;
            $this->senha = $_senha;
        }
    }
?>
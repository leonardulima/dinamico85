<?php
    require_once('../config.php');
    $ban = new Banner();
    // Inserindo
    if(isset($_POST['btn_cadastrar']))
    {
        var_dump($_FILES['img']);       
         $img = upload_imagem();
        $ban->insertBanner($_POST['txt_titulo'],$_POST['txt_link'],$img[0],$_POST['txt_caminho'],$_POST['txt_alt'],isset($_POST['check_ativo'])? '1':'0');
        if($ban->getId()>0)
        {
            header('location:principal.php?link=8&msg=ok');
        }
        else
        {
            header('location:principal.php?link=8&msg=erro');
        }
    }
    
    // Deletando 
    if(isset($_GET['delete']))
    {
        if($_GET['id'])
        {
            $ban->deleteBanner($_GET['id']);
            header('location:principal.php?link=9');
        }
    }
    // Atualizando
    if(isset($_GET['update']))
    {
        if (isset($_POST['btn_update']) && $_GET['update']==1) 
        {
            $imagem = upload_imagem();
            $ban->updateBanner($_POST['id'],$_POST['titulo'],$_POST['link'],$imagem[0],$_POST['alt'],isset($_POST['ativo'])?'s':'n');
            header('location:principal.php?link=9');
        }
        else if (isset($_POST['img_atual'])) 
        {
            $ban->updateBanner($_POST['id'],$_POST['titulo'],$_POST['link'],$_POST['img_atual'],$_POST['alt'],isset($_POST['ativo'])?'s':'n');
            header('location:principal.php?link=9');
        }
    }
    else 
    {
        header('location:principal.php?link=9$erro=1');
    }
?>
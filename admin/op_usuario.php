<?php
    require_once('../config.php');   
    $user = new Usuario(); 

        // Inserir usuario    
        if(isset($_POST['cadastrar_usuario']))
        {
             $user = new Usuario(
               0,
               $_POST['nome'],
               $_POST['email'],
               $_POST['senha']);
            if($_POST['senha'] == $_POST['txt_senha_confirmar'])
            {
                $user->insertUser();
                var_dump($user);
                header('location:../index.php');
            }
            else
             {
                header('location:../index.php?&msg=erro');
             }
        }
    // logando Usuario
    if(isset($_POST['btn_logar_usuario']))
    {
        if(isset($_POST['email_usuario']))        
        {            
            $user->logarUser($_POST['email_usuario'],$_POST['senha_usuario']);
            if($user->getId()>0)
            {
                $_SESSION['user_logado'] = true;
                $_SESSION['email_usuario'] = $user->getEmail();
                $_SESSION['nome_usuario'] = $user->getNome();
                $_SESSION['id_usuario'] = $user->getId();               
                var_dump($_SESSION);
                header('location:../index.php?&msg=Login efetuado com sucesso');
            }
            else
            {
                header('location:../index.php?msg= Email ou senha Incorreto!');
            }            
        }
    }
    // // Cadastrando Usuario
    // if(isset($_POST['cadastrar_usuario']))
    // {
    //     if(isset($_POST['nome_usuario']))
    //     {            
    //         $img = upload_imagem_user();
    //         $user->insertUser($_POST['nome_usuario'],$_POST['email_usuario'],$_POST['senha_usuario'],$img[0]);
    //         if($user->getId()>0)
    //         {
    //             header('location:index.php?link=7&msg=Cadastrado com Sucesso');                
    //         }
    //         else
    //         {
    //             header('location:index.php?link=7&msg=Erro ao cadastrar-se');                
    //         }
    //     }
    // }

    // Alterando Usuario
    if (isset($_POST['btn_alterar_usuario']))
    {
        $user = new Usuario();
        $user->update($_POST['id'],$_POST['nome'],$_POST['email']);
        header('location:principal.php?link=13');
        exit;
    }
    // Excluindo Usuario
    $id = filter_input(INPUT_GET,'id');
    $excluir = filter_input(INPUT_GET,'excluir');
    if(isset($id) && $excluir == 1)
    {        
        $user = new Usuario();
        var_dump($id);
        $user->delete($id);        
        header('location:principal.php?link=13');
        exit;
    }

    // Encerrando a sessão
    if(isset($_GET['sair']))
    {
        if($_GET['sair']=1)
        {
            $_SESSION['user_logado'] = false;
            $_SESSION['email_usuario'] = null;
            $_SESSION['nome_usuario'] = null;
            $_SESSION['id_usuario'] = null;
            header('location:../index.php');
            exit;
        }
    }

?>
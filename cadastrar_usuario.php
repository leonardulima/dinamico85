<!-- Usuario efetua o login -->

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Site Dinâmico - Cadastro</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div  id="box-cadastro">
        <div id="formulario-cadastro">
            <form id="frmlogin" name="frmlogin" action="admin/op_usuario.php" method="POST">
                <fieldset>
                    <legend>Faça Login</legend>
                    <br>
                    <label for=""><span>Nome</span></label>
                    <input type="text" name="nome" id="nome_usuario">
                    <br>
                    <br>
                    <label for=""><span>Email</span></label>
                    <input type="text" name="email" id="email_usuario">
                    <br>
                    <br>
                    <label for=""><span>Senha</span></label>
                    <input type="password" name="senha" id="senha_usuario">
                    <br>
                    <br>
                    <label for=""><span>Confirmar Senha</span></label>
                    <input type="password" name="txt_senha_confirmar" id="txt_senha_confirmar">
                    <br>
                    <br>
                    <input type="submit" name="cadastrar_usuario" id="cadastro_user" value="logar" class="botao">
                    <br>
                    <br>
                    <span>
                    <?php
                        if(isset($_GET['msg']))
                        {
                            if($_GET['msg']=='ok')
                                {
                                echo 'Cadastrado com sucesso !';
                                }
                                else if ($_GET['msg']=='errokey')
                            {
                                echo 'Erro s';
                            }
                            else
                            {
                                echo 'Erro';
                            }
                        }
                    ?>
                    </span>        
                </fieldset>
            </form>
        </div>
    </div>
    
</body>
</html>